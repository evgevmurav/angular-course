import {Component, forwardRef, OnInit, Provider} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

const VALUE__ACCESSOR: Provider = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => NgModelComponent),
  multi: true
};

@Component({
  selector: 'app-ng-model',
  templateUrl: './ng-model.component.html',
  styleUrls: ['./ng-model.component.scss'],
  providers: [VALUE__ACCESSOR]
})
export class NgModelComponent implements ControlValueAccessor {

  state = 'off';

  private onChange = (value: any) => {};

  setState(state: string): any {
    this.state = state;
    this.onChange(this.state);
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  writeValue(state: string): void {
    this.state = state;
  }


}
