import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-bases',
    templateUrl: './bases.component.html',
    styleUrls: ['./bases.component.scss']
})
export class BasesComponent implements OnInit {

    titleOne = 'BaseOne';
    titleTwo = 'BaseTwo12345';
    img = 'https://seeklogo.com/images/R/react-logo-7B3CE81517-seeklogo.com.png';
    inputValue = '';
    backgroundToggle = false;
    backgroundToggle2 = false;
    toggleTwo = false;
    toggle = false;
    arr = [1, 1, 2, 3, 5, 8];
    objs = [
        {
            title: 'Post 1', author: 'Petr', comments: [
                {name: 'Max', text: 'lorem 1'},
                {name: 'Max', text: 'lorem 2'},
                {name: 'Max', text: 'lorem 3'},
            ]
        },
        {
            title: 'Post 2', author: 'Ivan', comments: [
                {name: 'Max 2', text: 'lorem 1'},
                {name: 'Max 2', text: 'lorem 2'},
                {name: 'Max 2', text: 'lorem 3'},
            ]
        }
    ];
    now = new Date();


    constructor() {
        setTimeout(() => {
            this.img = 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Angular_full_color_logo.svg/1200px-Angular_full_color_logo.svg.png';
        }, 3000);
    }

    onInput(event): any {
        this.inputValue = event.target.value;
    }

    onClick(): any {
        console.log('click');
    }

    ngOnInit(): void {
    }

}
