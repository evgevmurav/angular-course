import {Component, OnInit} from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {MyValidators} from '../../validators/my.validators';

@Component({
    selector: 'app-forms',
    templateUrl: './forms.component.html',
    styleUrls: ['./forms.component.scss']
})
export class FormsComponent implements OnInit {

    form: FormGroup;

    constructor() {
    }

    ngOnInit(): void {
        this.form = new FormGroup({
            email: new FormControl('', [
                Validators.email,
                Validators.required,
                MyValidators.restrictedEmails],
                MyValidators.uniqEmail
                ),
            password: new FormControl(null, [
                Validators.required,
                Validators.minLength(6)]),
            address: new FormGroup({
                country: new FormControl('ru'),
                city: new FormControl('', Validators.required)
            }),
            skills: new FormArray([])
        });
    }

    submit(): any {
        if (this.form.valid) {
            console.log('form:', this.form);
            const formData = {...this.form.value};
            console.log('form data:', formData);
        }
        this.form.reset();
    }

    setCapital(): any {
        const cityMap = {
            ru: 'msc',
            ua: 'kv',
            by: 'ms'
        };
        const cityKey = this.form.get('address').get('country').value;
        const city = cityMap[cityKey];

        this.form.patchValue({
            address: {city}
        });
    }

    addSkill(): any {
        const control = new FormControl('', Validators.required);
        (this.form.get('skills') as FormArray).push(control);
    }
}
