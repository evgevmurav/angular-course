import { Component, OnInit } from '@angular/core';
import { AppCounterService } from 'src/app/services/app-counter.service';
import { LocalCounterService } from 'src/app/services/local-counter.service';


@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss'],
  providers: [LocalCounterService]
})
export class ServicesComponent implements OnInit {

  constructor(public appCounterService: AppCounterService,
              public localCounterService: LocalCounterService)
  { }

  ngOnInit(): void {
  }

}
