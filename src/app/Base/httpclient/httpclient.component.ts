import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {delay} from 'rxjs/operators';
import {Todo, TodosServices} from '../../services/todos.services';


@Component({
    selector: 'app-httpclient',
    templateUrl: './httpclient.component.html',
    styleUrls: ['./httpclient.component.scss']
})
export class HttpclientComponent implements OnInit {

    todos: Todo[] = [];
    todoTitle = '';
    loading = false;
    error = '';

    constructor(private todosService: TodosServices) {
    }

    ngOnInit(): void {
        this.fetchTodos();
    }

    addTodo(): any {
        if (!this.todoTitle.trim()) {
            return;
        }
        this.todosService.addTodo({
            title: this.todoTitle,
            completed: false
        }).subscribe(todo => {
            this.todos.push(todo);
            this.todoTitle = '';
        });
    }

    fetchTodos(): any {
        this.loading = true;
        this.todosService.fetchTodos()
            .subscribe(todos => {
                    this.todos = todos;
                    this.loading = false;
                }, error => {
                    this.error = error.message;
                }
            );
    }

    removeTodo(id: number): any {
        this.todosService.removeTodo(id)
            .subscribe(() => {
                this.todos = this.todos.filter(t => t.id !== id);
            });
    }

    completeTodo(id: number): any {
        this.todosService.completeTodo(id).subscribe(todo => {
            this.todos.find(t => t.id === todo.id).completed = true;
        });
    }
}
