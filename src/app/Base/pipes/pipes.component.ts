import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';

export interface Post {
    title: string;
    text: string;
}

@Component({
    selector: 'app-pipes',
    templateUrl: './pipes.component.html',
    styleUrls: ['./pipes.component.scss']
})
export class PipesComponent implements OnInit {

    e: number = Math.E;

    str = 'hello world';
    date = new Date();
    float = 0.42;
    obj = {
        a: 1,
        b: {
            c: 2,
            d: {
                e: 3,
                f: 4
            }
        }
    };

    search = '';
    searchField = 'title';

    addPost(): void {
        this.posts.unshift({
            title: 'angular',
            text: 'course'
        });
    }

    posts: Post[] = [
        {title: 'Beer', text: 'Best ever beer'},
        {title: 'Vodka', text: 'Best ever vodka'},
        {title: 'Water', text: 'Water ever vodka'},
    ];

    p: Promise<string> = new Promise<string>(resolve => {
        setTimeout(() => {
                resolve('promise Resolved');
            }, 4000
        );
    });

    date$: Observable<Date> = new Observable<Date>(obs => {
        setInterval(() => {
            obs.next(new Date());
        });
    });

    constructor() {
    }

    ngOnInit(): void {
    }

}
