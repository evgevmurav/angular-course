import { Component, OnInit} from '@angular/core';

export interface Post {
    title: string;
    text: string;
    id?: number;
}

@Component({
    selector: 'app-components',
    templateUrl: './components.component.html',
    styleUrls: ['./components.component.scss']
})
export class ComponentsComponent implements OnInit {

    posts: Post[] = [
        {title: 'Название карточки', text: 'Текс карточки', id: 1 },
        {title: 'Название карточки2', text: 'Текс карточки2', id: 2 },
    ];

    updatePosts(post: Post): any {
        this.posts.unshift(post);
    }

    removePost(id: number): any {
        this.posts = this.posts.filter(p => p.id !== id);
    }

    constructor() {}

    ngOnInit(): void {
    }

}
