import {BrowserModule} from '@angular/platform-browser';
import {NgModule, Provider} from '@angular/core';

import {AppComponent} from './app.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BasesComponent} from './Base/bases/bases.component';
import { ComponentsComponent } from './Base/components/components.component';
import { PostComponent } from './Base/components/post/post.component';
import { PostFormComponent } from './Base/components/post-form/post-form.component';
import {StyleDirective} from './directives/style.directive';
import { DirectivesComponent } from './Base/directives/directives.component';
import { IfNotDirective } from './directives/ifnot.directive';
import { PipesComponent } from './Base/pipes/pipes.component';
import {MultByPipe} from './pipes/mult-by';
import { ExMarksPipe } from './pipes/ex-marks.pipe';
import { FilterPipe } from './pipes/filter.pipe';
import { ServicesComponent } from './Base/services/services.component';
import { CounterComponent } from './Base/services/counter/counter.component';
import { FormsComponent } from './Base/forms/forms.component';
import { NgModelComponent } from './Base/ng-model/ng-model.component';
import { HttpclientComponent } from './Base/httpclient/httpclient.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {AuthInterceptor} from './inteceptor/auth.interceptor';

const INTERCEPTOR_PROVIDER: Provider = {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true
};

@NgModule({
    declarations: [
        AppComponent,
        BasesComponent,
        ComponentsComponent,
        PostComponent,
        PostFormComponent,
        StyleDirective,
        DirectivesComponent,
        IfNotDirective,
        PipesComponent,
        MultByPipe,
        ExMarksPipe,
        FilterPipe,
        ServicesComponent,
        CounterComponent,
        FormsComponent,
        NgModelComponent,
        HttpclientComponent,
    ],
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule
    ],
    providers: [INTERCEPTOR_PROVIDER],
    bootstrap: [AppComponent]
})
export class AppModule {
}
