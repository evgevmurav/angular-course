import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, delay} from 'rxjs/operators';

export interface Todo {
    completed: boolean;
    title: string;
    id?: number;
    userId?: number;
}

@Injectable({providedIn: 'root'})

export class TodosServices {
    constructor(private http: HttpClient) {

    }

    addTodo(todo: Todo): Observable<Todo> {
       return  this.http.post<Todo>('http://jsonplaceholder.typicode.com/todos', todo, {
           headers: new HttpHeaders({
               MyCustomHeader: Math.random().toString()
           })
       });
    }
    fetchTodos(): Observable<Todo[]> {
        return this.http.get<Todo[]>('http://jsonplaceholder.typicode.com/todos?_', {
            params: new HttpParams().set('_limit', '3')
        })
            .pipe(delay(500),
                catchError(error => {
                   console.log(error.message);
                   return throwError(error);
                })
            );
    }
    removeTodo(id: number): Observable<void> {
        return this.http.delete<void>(`http://jsonplaceholder.typicode.com/todos/${id}`);
    }
    completeTodo(id: number): Observable<Todo> {
        return  this.http.put<Todo>(`http://jsonplaceholder.typicode.com/todos/${id}`, {
            completed: true
        });
    }
}
