import {Directive, Input, TemplateRef, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[appIfNot]'
})
export class IfNotDirective {

  @Input('appIfNot') set ifNot(condition: boolean) {
    if (!condition) {
      // Показать
      this.ViewContainer.createEmbeddedView(this.templateRef);
    } else {
      // Скрыть
      this.ViewContainer.clear();
    }
  }

  constructor(private templateRef: TemplateRef<any>, private ViewContainer: ViewContainerRef) { }

}
