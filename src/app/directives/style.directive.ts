import {Directive, ElementRef, HostBinding, HostListener, Input, Renderer2} from '@angular/core';

@Directive({
    selector: '[appStyle]'
})

export class StyleDirective {

    @Input('appStyle') color = 'blue';
    @HostBinding('style.color') elColor = null;

    constructor(private el: ElementRef, private r: Renderer2) {

        this.r.setStyle(this.el.nativeElement, 'color', 'blue');

    }

    @HostListener('click', ['$event.target']) onClick(event: Event): any {

    }

    @HostListener('mouseenter') onEnter(): any {
        // this.r.setStyle(this.el.nativeElement, 'color', this.color);
        this.elColor = this.color;
    }

    @HostListener('mouseleave') onLeave(): any {
        // this.r.setStyle(this.el.nativeElement, 'color', null);
        this.elColor = null;
    }
}
